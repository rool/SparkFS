/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/FileSys/ImageFS/SparkFS/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.fx */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>



#include "kernel.h"
#include "swis.h"

#include "Interface/SparkFS.h"

#include "Consts.h"
#include "veneers.h"
#include "Interface.h"
#include "fsentries.h"
#include "ModuleWrap.h"

#include "flex.h"
#include "arcs.h"
#include "sfs.h"
#include "cat.h"
#include "fx.h"
#include "fs.h"
#include "link.h"
#include "scrap.h"
#include "sfserrs.h"
#include "buffer.h"
#include "callback.h"
#include "xstr.h"


/*****************************************************************************/




filestr *   fsmap[MAXFILES];  /* maps file strcuture number to cat + data */
filehandle  fmap[MAXFILES];   /* maps file handles to file structures */



_kernel_oserror * getnewfilehandle(int acn,int fn,int new,int * fh)
{
 _kernel_oserror * err;
 int i;
 int j;
 int fs;

 err=NULL;

 deb("getnewfilehandle in");

 for(i=1;i<MAXFILES;i++) if(!fmap[i].file) break;

 if(i==MAXFILES)
 {
  /* return too many files open error */
  err=geterror(mb_nfserr_TooManyFiles);
 }
 else
 {
  if(arc[acn].hdr[fn].fshandle)  /* the file is already open */
  {
   fmap[i].file=fs=arc[acn].hdr[fn].fshandle;
   fmap[i].pointer=0;
   fsmap[fs]->cat.open++;
   fmap[i].fswhandle=*fh;
  }
  else   /* we have to open the file */
  {
   for(j=1;j<MAXFILES;j++) if(!fsmap[j]) break;

   if(j==MAXFILES) { /* return too many files open error */ }

   if(!flex_alloc((flex_ptr)&fsmap[j],sizeof(filestr)))
   {
    /* return can't open error */
    err=geterror(mb_malloc_failed);
   }
   else
   {
    arc[acn].files++;
    fmap[i].file=arc[acn].hdr[fn].fshandle=j;
    fmap[i].pointer=0;
    fmap[i].fswhandle=*fh;

    fsmap[j]->cat.load=arc[acn].hdr[fn].load;

    fsmap[j]->cat.exec=arc[acn].hdr[fn].exec;
    fsmap[j]->cat.acc=arc[acn].hdr[fn].acc;
    fsmap[j]->cat.length=arc[acn].hdr[fn].length;
    fsmap[j]->cat.alloc=fsmap[j]->cat.length;
    fsmap[j]->cat.isdir=arc[acn].hdr[fn].dirn>=0 || arc[acn].hdr[fn].dirn==-2;
    fsmap[j]->cat.modded=0;
    fsmap[j]->cat.open=1;
    fsmap[j]->cat.state=OPENPEND;
    fsmap[j]->cat.fp=0;
   }
  }
  *fh=i;
 }


 deb("getnewfilehandle ex");

 return(err);
}



static void storefshdr(xheads * hdr,int fs)
{
 hdr->hdr.load=fsmap[fs]->cat.load;
 hdr->hdr.exec=fsmap[fs]->cat.exec;
 hdr->hdr.acc=fsmap[fs]->cat.acc;
 hdr->hdr.length=fsmap[fs]->cat.length;
}


void updatefsmap(int acn,int fn)
{
 int     fs;
 heads * hdr;

 hdr=&arc[acn].hdr[fn];

 if((fs=hdr->fshandle)!=0)
 {
  fsmap[fs]->cat.load=hdr->load;
  fsmap[fs]->cat.exec=hdr->exec;
  fsmap[fs]->cat.acc=hdr->acc;
 }
}



static void copyxhdrhdr(xheads * xhdr,heads * hdr)
{
 strcpy(xhdr->name,stringptr(hdr->name));
 xhdr->hdr=*hdr;
}


static _kernel_oserror * stashdata(int acn,int fn,int fs)
{
 _kernel_oserror * err;
 xheads            hdr;
 int               ind;

 copyxhdrhdr(&hdr,&arc[acn].hdr[fn]);
 storefshdr(&hdr,fs);
 ind=parentof(&arc[acn],fn);

 deb("stash data");

 err=savearcfile(acn,&hdr,fn,ind,1,fs,0);

 return(err);
}




_kernel_oserror * closefilehandle(int fh)
{
 _kernel_oserror * err;

 int fs;
 int i;
 int j;

 err=NULL;

 fs=fmap[fh].file;

 if(fsmap[fs]->cat.open==1)
 {
  /* last file handle open on file structure */

  for(i=0;i<MAXARC;i++)
  {
   if(arc[i].inuse && arc[i].files)
   {
    for(j=0;j<arc[i].nofiles;j++)
    {
     if(arc[i].hdr[j].fshandle==fs)
     {
      if(fsmap[fs]->cat.modded)
      {
       err=stashdata(i,j,fs);
      }

      arc[i].hdr[j].fshandle=0;
      arc[i].files--;
      callbackusefile(i);
     }
    }
   }
  }

  if(fsmap[fs]->cat.state==OPENDISC) closescrap(fsmap[fs]->cat.fp);

  flex_free((flex_ptr)&fsmap[fs]);
 }
 else
 {
  fsmap[fs]->cat.open--;
 }

 fmap[fh].file=0;

 return(err);
}





/* load/expand the data for the given file */

static _kernel_oserror * loadfiledata(int fs)
{
 _kernel_oserror * err;

 int i;
 int j;

 err=NULL;

 for(i=0;i<MAXARC;i++)
 {
  if(arc[i].inuse && arc[i].files)
  {
   for(j=0;j<arc[i].nofiles;j++)
   {
    if(arc[i].hdr[j].fshandle==fs)
    {
     err=loadarcfile(i,j,fs,NULL);
     callbackusefile(i);
    }
   }
  }
 }

 return(err);
}


/* probably should be part of above ... */

static _kernel_oserror * downloadfile(int fs)
{
 _kernel_oserror * err;

 /* have to load data */

 err=loadfiledata(fs);
 if(err) return(err);

 return(err);
}





_kernel_oserror * getbytes(int fh,char * dest,int n,int offset)
{
 int               fs;
 _kernel_oserror * err;


 err=NULL;

 fs=fmap[fh].file;

 if(fsmap[fs]->cat.state==OPENPEND) err=downloadfile(fs);

 if(!err)
 {
  if((offset+n)>fsmap[fs]->cat.length) n=fsmap[fs]->cat.length-offset;

/* deb("n=%d offset=%d len=%d data=%x",n,offset,fsmap[fs]->cat.length,
                                               fsmap[fs]->data.data); */

  if(fsmap[fs]->cat.state==OPENMEM)
  {
   memcpy(dest,(fsmap[fs]->data.data)+offset,n);
  }
  else
  if(fsmap[fs]->cat.state==OPENDISC)
  {
   err=readat(scrapmap[fsmap[fs]->cat.fp],dest,n,offset);
  }

  deb("gbx");
 }

 return(err);
}



_kernel_oserror * putouttodisc(int fs)
{
 _kernel_oserror * err;
 int               oldlen;
 int               fp;
 int               sfh;

 oldlen=fsmap[fs]->cat.length;

 err=openscrap('w',&fp);  /* use fp as temp */
 if(!err)
 {
  sfh=scrapmap[fp];

  err=write(sfh,fsmap[fs]->data.data,oldlen);

  if(!err)
  {
   flex_extend((flex_ptr)&fsmap[fs],sizeof(fh));
   fsmap[fs]->cat.state=OPENDISC;
   fsmap[fs]->cat.fp=fp;
  }
  else close(sfh);
 }
 return(err);
}


/* it is assumed that file actually exists at this point */

static _kernel_oserror * setextent(int fs,int newlen,int fill,int w)
{
 _kernel_oserror * err;
 int               oldlen;
 int               sfh;

 err=NULL;

 oldlen=fsmap[fs]->cat.length;

 if((newlen>oldlen) || w)
 {
  if(fsmap[fs]->cat.state==OPENMEM)
  {
   if(flex_extend((flex_ptr)(flex_ptr)&fsmap[fs],sizeof(fh)+newlen))
   {
    if(fill && (newlen>oldlen))
                     memset(fsmap[fs]->data.data+oldlen,0,newlen-oldlen);
   }
   else
   {
    /* can go out to disc... */
    err=putouttodisc(fs);
   }
  }

  if(fsmap[fs]->cat.state==OPENDISC && !err)
  {
   sfh=scrapmap[fsmap[fs]->cat.fp];
   err=setfileextent(sfh,newlen);
  }

  if(!err)
  {
   if(newlen!=oldlen) fsmap[fs]->cat.modded=1;
   fsmap[fs]->cat.length=newlen;
  }
 }

 return(err);
}




_kernel_oserror * putbytes(int fh,char * src,int n,int offset)
{
 int               fs;
 _kernel_oserror * err;


 err=NULL;

 fs=fmap[fh].file;

 if(fsmap[fs]->cat.state==OPENPEND) err=downloadfile(fs);

 if(!err)
 {
  err=setextent(fs,offset+n,0,0);
  if(!err)
  {
   fsmap[fs]->cat.modded=1;

   if(fsmap[fs]->cat.state==OPENMEM)
   {
    memcpy((fsmap[fs]->data.data)+offset,src,n);
   }
   else
   if(fsmap[fs]->cat.state==OPENDISC)
   {
    err=writeat(scrapmap[fsmap[fs]->cat.fp],src,n,offset);
   }
  }
 }


 return(err);
}




/* can flag errors outside file... if file is read only */

_kernel_oserror * validextent(int fh,int extent,int write)
{
 int               fs;
 _kernel_oserror * err;

 err=NULL;

 deb("extent=%d",extent);

 fs=fmap[fh].file;

 if(fsmap[fs]->cat.state==OPENPEND) err=downloadfile(fs);

 if(!err) err=setextent(fs,extent,1,write);

 return(err);
}




/* called by *dismount i.e. outside the FS to close all files */

_kernel_oserror * closeallfiles(int acn)
{
 int i;
 int j;
 int fs;

 for(i=0;i<arc[acn].files;i++)
 {
  if((fs=arc[acn].hdr[i].fshandle)!=0)
  {
   for(j=1;j<MAXFILES;j++)
   {
    if(fmap[j].file==fs) close(fmap[j].fswhandle);
   }
  }
 }

 return(NULL);
}




int sfsfilehandle(int fswhandle)
{
 int i;

 for(i=1;i<MAXFILES;i++)
 {
  if(fmap[i].file && fmap[i].fswhandle==fswhandle) return(i);
 }

 return(0);
}



_kernel_oserror * validatearchivefh(int acn)
{
 _kernel_oserror * err;
 int               fh;
 int               fs;

 err=NULL;

 if(arc[acn].image)
 {
  fh=sfsfilehandle(arc[acn].fh);

  if(fh)
  {
   fs=fmap[fh].file;
   if(fsmap[fs]->cat.state==OPENPEND) err=downloadfile(fs);
   if(!err && fsmap[fs]->cat.state!=OPENDISC) err=putouttodisc(fs);
  }
 }

 return(err);
}



_kernel_oserror * testarchivefh(int acn,int r,int w)
{
 _kernel_swi_regs  rx;
 _kernel_oserror * err;

 err=NULL;

 if(arc[acn].image)
 {
  rx.r[0]=254;
  rx.r[1]=arc[acn].fh;

  err=_kernel_swi(OS_Args,&rx,&rx);
  if(!err)
  {
   if((r && !(rx.r[0] & (1<<6))) || (w && !(rx.r[0] & (1<<7))))
                                        err=geterror(mb_sfserr_Access);
  }
 }
 return(err);
}


