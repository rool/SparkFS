/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/FileSys/ImageFS/SparkFS/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.scrap */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>



#include "kernel.h"
#include "swis.h"

#include "Interface/SparkFS.h"

#include "Consts.h"
#include "veneers.h"
#include "Interface.h"
#include "fsentries.h"
#include "ModuleWrap.h"


#include "fs.h"
#include "fx.h"
#include "sfs.h"
#include "sfserrs.h"
#include "scrap.h"

/*****************************************************************************/

int scrapmap[MAXFILES];      /* map scrap number to file handle */



static char * scrappath="cdir <Spark$Scrap>.SparkFSTmp";

static _kernel_oserror * validscrap(void)
{
 _kernel_oserror * err;

 err=NULL;
 if(!fexists(scrappath+5)) err=oscli(scrappath);
 return(err);
}




static void scrapname(int i,char * string)
{
 sprintf(string,"<Spark$Scrap>.SparkFSTmp.SFSX%02d",i);
}


/* get a new scrap file number */

static int getscrapn(void)
{
 int  i;
 char string[64];
 int  code;

 for(i=1;i<MAXFILES;i++)
 {
  if(!scrapmap[i])
  {
   scrapname(i,string);
   code=fexists(string);
   if(!code) return(i);
  }
 }

 return(-1);
}




/* opens a scrap file   */
/* returns handle in fp */

_kernel_oserror * openscrap(int mode,int * fp)
{
 _kernel_oserror * err;
 int               i;
 char              string[64];

 err=validscrap();

 if(!err)
 {
  i=getscrapn();

  if(i==-1) err=geterror(mb_sfserr_BadScrap);
  else
  {
   scrapname(i,string);
   err=open(string,mode,&scrapmap[i]);
   if(!err && !scrapmap[i]) err=geterror(mb_sfserr_BadScrap);

   *fp=i;
  }
 }

 return(err);
}


_kernel_oserror * closescrap(int fp)
{
 _kernel_oserror * err;
 char              string[64];

 err=close(scrapmap[fp]);

 scrapname(fp,string);
 delete(string);
 scrapmap[fp]=0;

 return(err);
}



_kernel_oserror * closescrapx(int fh)
{
 _kernel_oserror * err;
 int               i;

 err=NULL;

 for(i=0;i<MAXFILES;i++)
  if(scrapmap[i]==fh) err=closescrap(i);

 return(err);
}


_kernel_oserror * scrapinit(void)
{
 int i;

 for(i=0;i<MAXFILES;i++) scrapmap[i]=0;

 return(NULL);
}



void scrapfinit(void)
{
 fs_wipe(scrappath+5);
}


