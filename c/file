/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/FileSys/ImageFS/SparkFS/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.file */

#include <stdio.h>
#include <stdlib.h>


#include "kernel.h"
#include "swis.h"

#include "Interface/SparkFS.h"

#include "Consts.h"
#include "veneers.h"
#include "Interface.h"
#include "fsentries.h"
#include "ModuleWrap.h"

#include "fs.h"
#include "mem.h"
#include "sfs.h"
#include "arcs.h"
#include "image.h"
#include "sff.h"
#include "sfserrs.h"
#include "callback.h"



static _kernel_oserror *  formpath(FSEntry_File_Parameter * parm,
                                          char ** path,char ** name)
{
 return(makepath(parm->name,parm->special.special_field,path,name));
}


static _kernel_oserror *  imformpath(FSEntry_File_Parameter * parm,int fsfh,
                                            char ** path,char ** name)
{
 return(immakepath(parm->name,fsfh,path,name));
}




_kernel_oserror *fsentry_file(FSEntry_File_Parameter * parm)
{
 _kernel_oserror * err;
 int               acn;
 char            * arcpath;
 char            * arcname;


 deb("file reason=%d",parm->reason);

 err=memcheck();
 if(!err)
 {
  err=formpath(parm,&arcpath,&arcname);
  if(!err)
  {
   if(!err) err=gethandle(arcname,&acn);
   if(!err) err=truncatepath(arcpath,acn,fstruncate);

   if(!err)

   switch(parm->reason)
   {
        case FSEntry_File_Reason_LoadFile:
                err=LoadFile(acn,arcpath,parm);
                break;

        case FSEntry_File_Reason_CreateFile:
                err=CreateFile(acn,arcpath,parm);
                break;

        case FSEntry_File_Reason_SaveFile:
                err=SaveFile(acn,arcpath,parm);
                break;

        case FSEntry_File_Reason_WriteCatalogueInformation:
                err=WriteCatalogueInformation(acn,arcpath,parm);
                break;


#ifdef OLDFS

        case FSEntry_File_Reason_WriteLoadAddress:
                err=WriteLoadAddress(acn,parm);
                break;

        case FSEntry_File_Reason_WriteExecutionAddress:
                err=WriteExecutionAddress(acn,parm);
                break;

        case FSEntry_File_Reason_WriteAttributes:
                err=WriteAttributes(acn,parm);
                break;

        case FSEntry_File_Reason_ReadCatalogueInformationNoLength:
                err=ReadCatalogueInformationNoLength(acn,parm);
                break;

#endif


        case FSEntry_File_Reason_ReadCatalogueInformation:
                err=ReadCatalogueInformation(acn,arcpath,parm);
                break;

        case FSEntry_File_Reason_DeleteObject:
                err=DeleteObject(acn,arcpath,parm);
                break;

        case FSEntry_File_Reason_CreateDirectory:
                err=CreateDirectory(acn,arcpath,parm);
                break;

        default:
                err=geterror(mb_nfserr_BadParameters);
                break;
   }
   clearpath(arcpath,arcname);
  }
 }
 return(err);
}




_kernel_oserror *imentry_file(FSEntry_File_Parameter * parm)
{
 _kernel_oserror *err;
 int              acn;
 int              fsfh;
 char           * arcpath;
 char           * arcname;


 deb("im file reason=%d",parm->reason);

 err=memcheck();

 if(!err)
 {
  err=imagegetfsfh(parm->special.imfh,&fsfh);
  if(!err)
  {
   err=imformpath(parm,fsfh,&arcpath,&arcname);
   if(!err)
   {
    deb("imfh=%d fsfh=%d\n",parm->special.imfh,fsfh);
    if(!err)
    {
     err=imgethandle(arcname,fsfh,&acn);
     if(!err) err=truncatepath(arcpath,acn,fstruncate);
     deb("acn=%d",acn);
     if(!err)
     {
      switch(parm->reason)
      {
         case FSEntry_File_Reason_LoadFile:
                 err=LoadFile(acn,arcpath,parm);
                 break;

         case FSEntry_File_Reason_CreateFile:
                 err=CreateFile(acn,arcpath,parm);
                 break;

         case FSEntry_File_Reason_SaveFile:
                 err=SaveFile(acn,arcpath,parm);
                 break;

         case FSEntry_File_Reason_WriteCatalogueInformation:
                 err=WriteCatalogueInformation(acn,arcpath,parm);
                 break;

         case FSEntry_File_Reason_ReadCatalogueInformation:
                 err=ReadCatalogueInformation(acn,arcpath,parm);
                 break;

         case FSEntry_File_Reason_DeleteObject:
                 err=DeleteObject(acn,arcpath,parm);
                 break;

         case FSEntry_File_Reason_CreateDirectory:
                 err=CreateDirectory(acn,arcpath,parm);
                 break;

         default:
                 err=geterror(mb_nfserr_BadParameters);
                 break;
      }
     }
    }
    clearpath(arcpath,arcname);
   }
  }
 }

 callbackscan();

 return(err);
}


